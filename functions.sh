wi_die() {
	echo >&2 $1
    cd $LOCATION
	exit 2
}

option_check() {
    if [[ "$1" -lt 2 ]]
    then echo "option requires argument: ${2:-}"; exit 2;
    else return 0
    fi
}

wi_get_property() {
    [[ ! $# -eq 2 ]] && {
        wi_die 'wrong # args: should be: wi_get_property property_file property'
    }
    local -r property_file=$1 property=$2
    [[ ! -f ${property_file} ]] && wi_die "metadata not found: ${property_file}"
    grep "^$property=" ${property_file} >/dev/null && {
        set +u
        ( . ${property_file} ;
            eval echo \$${property:-} )
        set -u
        return 0
    }
    return $?
}

wi_link_tool() {
    [[ ! $# -eq 2 ]] && {
        wi_die 'wrong # args: should be: wi_link_tool target destination'
    }

    unlink $2
    ln -s $1 $2 || wi_die "enable to link tool"
}

wi_download() {
    [[ ! $# -eq 2 ]] && {
        wi_die 'wrong # args: should be: wi_download url target'
    }
    if [ ! -f $2 ]; then
        if [ -f "$2.tmp" ]; then
            rm "$2.tmp"
        fi
        wget -q -O "$2.tmp" $1 || wi_die "unable to download $1"
        mv "$2.tmp" $2
    fi
}

wi_log_start() {
    echo " >> $1"
}

wi_log_step() {
    echo "    | $1"
}

wi_log_success() {
    echo "    -> $1"
    echo " "
}
