#!/usr/bin/env bash

. $LOCATION/functions.sh || { 
  echo >&2 "Failed loading function library." ; exit 1 ; 
}

VERSION=

parse_module_options() {
    while [ "$#" -gt 0 ]; do
        OPT="$1"
        case "$OPT" in
            --version) option_check $# $1; VERSION=$2 ; shift ;;
            *)
              #break
        esac
        shift
    done

    [ -z "$VERSION" ] && { echo >&2 "missing required option: --version" ; exit 2 ; }
}

parse_module_options "$@"

wi_log_start "installing sbt-${VERSION} ..."

if [ ! -d $DIRECTORY/tools/sbt-${VERSION} ]; then
  wi_log_step "downloading sbt-${VERSION} ..."
  wi_download http://repo.scala-sbt.org/scalasbt/sbt-native-packages/org/scala-sbt/sbt/${VERSION}/sbt.tgz $CACHE/sbt-${VERSION}.tgz

  wi_log_step "extracting sbt-${VERSION} to tools directory"
  tar -xzf $CACHE/sbt-${VERSION}.tgz -C $DIRECTORY/tools || wi_die "unable to extract sbt $VERSION"
  mv $DIRECTORY/tools/sbt $DIRECTORY/tools/sbt-${VERSION}
fi


wi_log_step "creating sbt-${VERSION} alias in workspace tools"
wi_link_tool $DIRECTORY/tools/sbt-${VERSION} $WORKSPACE_TOOLS/sbt

wi_log_step "writing sbt-${VERSION} rc file to workspace"

echo "export SBT_HOME=\$WORKSPACE_TOOLS/sbt" > $WORKSPACE_RC/sbt.rc
echo "export PATH=\$SBT_HOME/bin:\$PATH" >> $WORKSPACE_RC/sbt.rc

wi_log_success "sbt-${VERSION} installed successfully"

exit $?
