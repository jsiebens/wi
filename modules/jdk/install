#!/usr/bin/env bash

. $LOCATION/functions.sh || { 
  echo >&2 "Failed loading function library." ; exit 1 ; 
}

VERSION=

parse_module_options() {
    while [ "$#" -gt 0 ]; do
        OPT="$1"
        case "$OPT" in
            --version) option_check $# $1; VERSION=$2 ; shift ;;
            *)
              #break
        esac
        shift
    done

    [ -z "$VERSION" ] && { echo >&2 "missing required option: --version" ; exit 2 ; }
}

parse_module_options "$@"

URL=$(wi_get_property "$MODULE_LOCATION/metadata" "URL_${VERSION}")
FILE=$(wi_get_property "$MODULE_LOCATION/metadata" "FILE_${VERSION}")
HOME=$(wi_get_property "$MODULE_LOCATION/metadata" "HOME_${VERSION}")

[ -z "$HOME" ] && { echo >&2 "invalid version" ; exit 2 ; }

wi_log_start "installing ${HOME} ..."

if [ ! -d $DIRECTORY/tools/${HOME} ]; then
  if [ ! -f $CACHE/$FILE ]; then
    wi_log_step "downloading ${HOME} ..."
    wget -q -O $CACHE/$FILE --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" $URL || wi_die "unable to download $NAME"
  fi

  wi_log_step "extracting ${FILE} to tools directory"

  extension="${FILE##*.}"
  
  case "$extension" in
    gz)
      tar -xzf $CACHE/$FILE -C $DIRECTORY/tools || wi_die "unable to extract $NAME"
      ;;
    bin)
      cp $CACHE/$FILE $DIRECTORY/tools/$FILE;
      chmod +x $DIRECTORY/tools/$FILE;
      here=`pwd`;
      cd $DIRECTORY/tools;
      ./$FILE >/dev/null;
      rm -f $FILE
      cd $here
      ;;
    *)
      wi_die "unknown file extension $extension"
      ;;
  esac
 
fi

wi_log_step "creating ${HOME} alias in workspace tools"
wi_link_tool $DIRECTORY/tools/${HOME} $WORKSPACE_TOOLS/jdk

wi_log_step "writing ${HOME} rc file to workspace"

echo "export JAVA_HOME=\$WORKSPACE_TOOLS/jdk" > $WORKSPACE_RC/jdk.rc
echo "export PATH=\$JAVA_HOME/bin:\$PATH" >> $WORKSPACE_RC/jdk.rc

wi_log_success "${HOME} installed successfully"

exit $?
