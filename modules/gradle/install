#!/usr/bin/env bash

. $LOCATION/functions.sh || { 
  echo >&2 "Failed loading function library." ; exit 1 ; 
}

VERSION=

parse_module_options() {
    while [ "$#" -gt 0 ]; do
        OPT="$1"
        case "$OPT" in
            --version) option_check $# $1; VERSION=$2 ; shift ;;
            *)
              #break
        esac
        shift
    done

    [ -z "$VERSION" ] && { echo >&2 "missing required option: --version" ; exit 2 ; }
}

parse_module_options "$@"

wi_log_start "installing gradle-${VERSION} ..."

if [ ! -d $DIRECTORY/tools/gradle-${VERSION} ]; then
  wi_log_step "downloading gradle-${VERSION} ..."
  wi_download http://services.gradle.org/distributions/gradle-${VERSION}-bin.zip $CACHE/gradle-${VERSION}-bin.zip

  wi_log_step "extracting gradle-${VERSION} to tools directory"
  unzip -q $CACHE/gradle-${VERSION}-bin.zip -d $DIRECTORY/tools || wi_die "unable to extract gradle $VERSION"
fi


wi_log_step "creating gradle-${VERSION} alias in workspace tools"
wi_link_tool $DIRECTORY/tools/gradle-${VERSION} $WORKSPACE_TOOLS/gradle

wi_log_step "writing gradle-${VERSION} rc file to workspace"

echo "export GRADLE_HOME=\$WORKSPACE_TOOLS/gradle" > $WORKSPACE_RC/gradle.rc
echo "export PATH=\$GRADLE_HOME/bin:\$PATH" >> $WORKSPACE_RC/gradle.rc

wi_log_success "gradle-${VERSION} installed successfully"

exit $?
